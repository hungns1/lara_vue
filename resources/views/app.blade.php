<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Demo lara vue</title>
        @vite('resoures/js/app.js')
        @inertiaHead
    </head>
    <body class="antialiased">
       @inertia
    </body>
</html>
