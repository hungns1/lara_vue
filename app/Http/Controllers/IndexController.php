<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class IndexController extends BaseController
{
    public function index(){
        return inertia(
            'Index/Index', 
            [
                'message' => 'Hello world!'
            ]
            );
    }

    public function show(){
        return inertia(
            'Index/Show', 
            [
                'message' => 'Show!'
            ]
            );
    }
}
